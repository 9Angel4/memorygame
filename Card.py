import pygame, os

from pygame import Surface


class Card():
    revealed = False
    activeRect = None
    originalSize = (0, 0)

    def __init__(self, sprite, spriteIndex, width, height):
        super().__init__()
        self.sprite = sprite
        self.spriteIndex = spriteIndex
        self.originalSize = (width, height)
        self.surface = Surface((sprite.cellWidth, sprite.cellHeight))
        sprite.draw(self.surface, self.spriteIndex, 0, 0)
        # self.surface = pygame.transform.scale(self.surface, (width, height))
        self.coverSurface = Surface((width, height))
        self.coverSurface.fill((200, 200, 200))

    def getSize(self):
        return self.sprite.cellWidth, self.sprite.cellHeight

    def isEqualTo(self, cardToCompare):
        return self.spriteIndex == cardToCompare.spriteIndex

    def getCardSurface(self):
        if (self.revealed):
            return self.surface

        return self.coverSurface

    def setRevealed(self, revealed=False):
        self.revealed = revealed

    def resizeView(self, size):
        self.surface = pygame.transform.scale(self.surface, size)
        self.coverSurface = pygame.transform.scale(self.coverSurface, size)
