import pygame, sys, time, os, random, math

from pygame import Surface
from pygame.locals import *
from Card import Card
from SpriteSheet import SpriteSheet

GAME_TITLE = 'Disney memory'
FPS = 60
FPS_CLOCK = 0
WINDOW_WIDTH = 1360
WINDOW_HEIGHT = 920
MIN_CARD_PADDING = 10
WINDOW: Surface

#            R    G    B
GRAY       = (100, 100, 100)
LIGHT_GRAY = (200, 200, 200)
NAVYBLUE   = ( 60,  60, 100)
WHITE      = (255, 255, 255)
RED        = (255,   0,   0)
GREEN      = (  0, 255,   0)
BLUE       = (  0,   0, 255)
YELLOW     = (255, 255,   0)
ORANGE     = (255, 128,   0)
PURPLE     = (255,   0, 255)
CYAN       = (  0, 255, 255)
BG_COLOR = (120, 120, 200)

ALL_CARDS = list()
CURRENT_CARDS = list()
REVEALED_CARDS = list()
CARD_WIDTH, CARD_HEIGHT = 0, 0

GAME_STATES = {
    'startScreen': 0,
    'runningGame': 1,
    'gameOver': 2
}
GAME_STATE = 0
GAME_MODES: dict
TEXT_FONT: pygame.font.Font
CARD_COUNT = 4

def main():
    global FPS_CLOCK, WINDOW, TEXT_FONT

    # Init game env
    pygame.init()
    WINDOW = pygame.display.set_mode((WINDOW_WIDTH, WINDOW_HEIGHT), pygame.RESIZABLE)
    pygame.display.set_caption(GAME_TITLE)
    FPS_CLOCK = pygame.time.Clock()
    TEXT_FONT = pygame.font.Font('freesansbold.ttf', 150)

    # start game
    while True:
        showStartScreen()
        runGame()
        showGameOverScreen()


def printScreen():
    if GAME_STATE == GAME_STATES['startScreen']:
        printStartItems()
    elif GAME_STATE == GAME_STATES['runningGame']:
        printBoard()
    elif GAME_STATE == GAME_STATES['gameOver']:
        printGameOver()


def printStartItems():
    global WINDOW, GAME_MODES

    WINDOW.fill(BG_COLOR)
    titleSurface = TEXT_FONT.render('Memory Disney!', True, WHITE)
    titleRect = titleSurface.get_rect(center=(WINDOW_WIDTH / 2, 20 + titleSurface.get_height() / 2))
    WINDOW.blit(titleSurface, titleRect)

    sidePadding = 150
    modeSurfaceHeight = modeSurfaceWidth = int((WINDOW_WIDTH - (sidePadding * 2)) / len(GAME_MODES))
    font = pygame.font.Font('freesansbold.ttf', 15)

    for mode in GAME_MODES.items():
        surface = Surface((modeSurfaceWidth - 50, modeSurfaceHeight - 50))
        surface.fill(mode[1]['color'])
        textSurface = font.render(mode[1]['text'], True, WHITE)
        textSurfaceRect = textSurface.get_rect(center=(surface.get_width() / 2, surface.get_height() / 2))

        surface.blit(textSurface, textSurfaceRect)
        surfaceRect = surface.get_rect(left=(sidePadding + 25 + modeSurfaceWidth * mode[0]), centery=(WINDOW_HEIGHT / 2))
        WINDOW.blit(surface, surfaceRect)
        mode[1]['rect'] = surfaceRect


def printBoard():
    global WINDOW, ALL_CARDS, CARD_WIDTH, CARD_HEIGHT
    WINDOW.fill(BG_COLOR)

    colCount, rowCount = calculateRowCol(len(ALL_CARDS))

    cardWidth = CARD_WIDTH
    cardHeight = CARD_HEIGHT
    maxCardWidth = (WINDOW_WIDTH - 2 * MIN_CARD_PADDING * colCount) / colCount
    maxCardHeight = (WINDOW_HEIGHT - 2 * MIN_CARD_PADDING * rowCount) / rowCount
    if CARD_HEIGHT > maxCardHeight or CARD_WIDTH > maxCardWidth:
        heightRatio = maxCardHeight / CARD_HEIGHT
        widthRatio = maxCardWidth / CARD_WIDTH
        cardHeight = maxCardHeight if heightRatio < widthRatio else CARD_HEIGHT * widthRatio
        cardWidth = maxCardWidth if heightRatio >= widthRatio else CARD_WIDTH * heightRatio
    cardWidth = int(cardWidth)
    cardHeight = int(cardHeight)

    widthPadding = (WINDOW_WIDTH / colCount) - cardWidth
    heightPadding = (WINDOW_HEIGHT / rowCount) - cardHeight
    cardWidthPadding = cardWidth + widthPadding
    cardHeightPadding = cardHeight + heightPadding

    cardIndex = 0
    for card in ALL_CARDS:
        card.resizeView((cardWidth, cardHeight))
        x = cardIndex % colCount * cardWidthPadding
        y = int(cardIndex / colCount) * cardHeightPadding
        card.activeRect = WINDOW.blit(card.getCardSurface(), (x + widthPadding / 2, y + heightPadding / 2))
        cardIndex += 1


def printGameOver():
    global WINDOW, ALL_CARDS

    WINDOW.fill(BG_COLOR)
    titleSurface = TEXT_FONT.render('Bravo !!!', True, WHITE)
    titleRect = titleSurface.get_rect(center=(WINDOW_WIDTH / 2, 20 + titleSurface.get_height() / 2))
    WINDOW.blit(titleSurface, titleRect)

    printCards()


def showStartScreen():
    global FPS_CLOCK, WINDOW, GAME_STATE, GAME_MODES, CARD_COUNT

    GAME_STATE = GAME_STATES['startScreen']

    GAME_MODES = {
        0: {'cardCount': 4, 'color': GREEN, 'text': 'Facile', 'rect': None},
        1: {'cardCount': 7, 'color': ORANGE, 'text': 'Moyen', 'rect': None},
        2: {'cardCount': 10, 'color': RED, 'text': 'Difficile', 'rect': None},
        3: {'cardCount': 15, 'color': PURPLE, 'text': 'Impossible', 'rect': None},
    }
    printScreen()

    mouseClicked = False
    mouseX, mouseY = None, None
    while True:
        events = checkGlobalEvents()
        for event in events:
            if event.type == MOUSEMOTION:
                mouseX, mouseY = event.pos
            elif event.type == MOUSEBUTTONUP:
                mouseX, mouseY = event.pos
                mouseClicked = True

        clickedItem = None
        if mouseClicked:
            mouseClicked = False
            clickedItem = getClickedStartItem(GAME_MODES, (mouseX, mouseY))

        if clickedItem is not None:
            CARD_COUNT = clickedItem['cardCount']
            return

        pygame.display.update()
        FPS_CLOCK.tick(FPS)


def showGameOverScreen():
    global FPS_CLOCK, WINDOW, GAME_STATE

    GAME_STATE = GAME_STATES['gameOver']
    printScreen()

    directions = [
        (-1, -1), (-0.5, -1), (0, -1), (0.5, -1), (1, -1),
        (-1, -0.5), (-0.5, -0.5), (0, -0.5), (0.5, -0.5), (1, -0.5),
        (-1, 0), (-0.5, 0), (0.5, 0), (1, 0),
        (-1, 0.5), (-0.5, 0.5), (0, 0.5), (0.5, 0.5), (1, 0.5),
        (-1, 1), (-0.5, 1), (0, 1), (0.5, 1), (1, 1)
    ]
    cardsDirection = {}

    while True:
        events = checkGlobalEvents()
        for event in events:
            if event.type == MOUSEBUTTONUP:
                return

        # Move cards
        cardIndex = 0
        for card in ALL_CARDS:
            if cardIndex not in cardsDirection.keys():
                multiplicator = random.randint(1, 5)
                direction = directions[random.randint(0, len(directions) - 1)]
                cardsDirection[cardIndex] = (direction[0]*multiplicator, direction[1]*multiplicator)

            card.activeRect.move_ip(cardsDirection[cardIndex][0], cardsDirection[cardIndex][1])

            if card.activeRect.x <= 0 or card.activeRect.x >= (WINDOW_WIDTH - CARD_WIDTH):
                cardsDirection[cardIndex] = (cardsDirection[cardIndex][0]*-1, cardsDirection[cardIndex][1])
            if card.activeRect.y <= 0 or card.activeRect.y >= (WINDOW_HEIGHT - CARD_HEIGHT):
                cardsDirection[cardIndex] = (cardsDirection[cardIndex][0], cardsDirection[cardIndex][1]*-1)

            cardIndex += 1

        printScreen()

        pygame.display.update()
        FPS_CLOCK.tick(FPS)


def runGame():
    global FPS_CLOCK, WINDOW, GAME_STATE, CARD_COUNT

    GAME_STATE = GAME_STATES['runningGame']

    cards = initGameBoard(CARD_COUNT)
    printScreen()

    mouseClicked = False
    mouseX, mouseY = None, None

    while True:
        events = checkGlobalEvents()
        for event in events:
            if event.type == MOUSEMOTION:
                mouseX, mouseY = event.pos
            elif event.type == MOUSEBUTTONUP:
                mouseX, mouseY = event.pos
                mouseClicked = True

        clickedCard = None
        if mouseClicked:
            clickedCard = getClickedCard(cards, (mouseX, mouseY))

        if clickedCard is not None and clickedCard.revealed is False:
            clickedCard.setRevealed(True)
            CURRENT_CARDS.append(clickedCard)

        # Update screen
        printCards()
        pygame.display.update()

        # Check after printed to reset view on next tick
        checkCards()
        mouseClicked = False

        FPS_CLOCK.tick(FPS)

        if len(REVEALED_CARDS) == len(ALL_CARDS):
            return


def checkCards():
    if len(CURRENT_CARDS) < 2:
        return

    if CURRENT_CARDS[0].isEqualTo(CURRENT_CARDS[1]):
        # Valid
        REVEALED_CARDS.append(CURRENT_CARDS[0])
        REVEALED_CARDS.append(CURRENT_CARDS[1])
    else:
        # Invalid
        CURRENT_CARDS[0].setRevealed(False)
        CURRENT_CARDS[1].setRevealed(False)
        pygame.time.wait(1000)
    CURRENT_CARDS.clear()


def getClickedCard(cards, mousePosition):
    for card in cards:
        if card.activeRect.collidepoint(mousePosition[0], mousePosition[1]):
            return card

    return None


def getClickedStartItem(gameModes, mousePosition):
    for item in gameModes.values():
        if "rect" in item and item['rect'] is not None and item['rect'].collidepoint(mousePosition[0], mousePosition[1]):
            return item

    return None


def initGameBoard(cardToPick):
    global CARD_WIDTH, CARD_HEIGHT, ALL_CARDS

    if cardToPick > 15:
        cardToPick = 15

    sprite = SpriteSheet(os.path.join('Resources', 'Cards', 'princess-sprite.jpg'), 4, 4, 15)
    indexes = [x for x in range(15)]
    random.shuffle(indexes)

    CARD_WIDTH = sprite.cellWidth
    CARD_HEIGHT = sprite.cellHeight

    resetCards()
    for i in range(cardToPick):
        ALL_CARDS.append(Card(sprite, indexes[i], sprite.cellWidth, sprite.cellHeight))
        ALL_CARDS.append(Card(sprite, indexes[i], sprite.cellWidth, sprite.cellHeight))
    random.shuffle(ALL_CARDS)

    return ALL_CARDS


def resetCards():
    global ALL_CARDS, CURRENT_CARDS, REVEALED_CARDS

    ALL_CARDS = list()
    CURRENT_CARDS = list()
    REVEALED_CARDS = list()


def printCards():
    global ALL_CARDS

    cardIndex = 0
    for card in ALL_CARDS:
        card.activeRect = WINDOW.blit(card.getCardSurface(), card.activeRect)
        cardIndex += 1


def calculateRowCol(totalCardCount):
    config = {
        2: [2, 1],
        4: [2, 2],
        6: [3, 2],
        8: [4, 2],
        10: [4, 3],
        12: [4, 3],
        14: [4, 4],
        16: [4, 4],
        18: [6, 3],
        20: [5, 4],
        22: [6, 4],
        24: [6, 4],
        26: [7, 4],
        28: [7, 4],
        30: [6, 5],
    }

    rowCount = 5
    colCount = math.ceil(totalCardCount/rowCount)
    if (totalCardCount in config):
        colCount, rowCount = config[totalCardCount][0], config[totalCardCount][1]

    return colCount, rowCount


def checkGlobalEvents():
    otherEvents = list()
    for event in pygame.event.get(): # event handling loop
        if event.type == QUIT or (event.type == KEYUP and event.key == K_ESCAPE):
            exitGame()
        elif event.type == pygame.VIDEORESIZE:
            resizeWindow(event.size, event.w, event.h)
        else:
            otherEvents.append(event)

    return otherEvents


def resizeWindow(size, width, height):
    global WINDOW, WINDOW_WIDTH, WINDOW_HEIGHT

    WINDOW_WIDTH, WINDOW_HEIGHT = width, height
    WINDOW = pygame.display.set_mode((WINDOW_WIDTH, WINDOW_HEIGHT), pygame.RESIZABLE)
    WINDOW.fill(BG_COLOR)
    printScreen()
    pygame.display.update()


def exitGame():
    pygame.quit()
    sys.exit(0)


if __name__ == '__main__':
    main()
