import pygame


class SpriteSheet:
    def __init__(self, filepath, cols, rows, cellCount):
        self.sheet = pygame.image.load(filepath)

        self.cols = cols
        self.rows = rows
        self.cellCount = cellCount

        self.rect = self.sheet.get_rect()

        w = self.cellWidth = self.rect.width / cols
        h = self.cellHeight = self.rect.height / rows
        hw, hh = self.cellCenter = (w/2, h/2)

        self.cells = list([(index % cols * w, int(index / cols) * h, w, h) for index in range(self.cellCount)])
        self.handle = list([
            (0, 0), (-hw, 0), (-w, 0),
            (0, -hh), (-hw, -hh), (-w, -hh),
            (0, -h), (-hw, -h), (-w, -h),
        ])


    def draw(self, surface, cellIndex, x, y, handle = 0):
        surface.blit(self.sheet, (x + self.handle[handle][0], y + self.handle[handle][1]), self.cells[cellIndex])

    def getImageZone(self, cellIndex):
        return self.cells[cellIndex]
